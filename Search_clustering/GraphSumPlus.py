# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def GraphSumPlus(A=None,varargin=None,*args,**kwargs):
    varargin = GraphSumPlus.varargin
    nargin = GraphSumPlus.nargin

    
    # Input: 
#   k-partite graph A, given as cell array with matrix A{i,j}=A{j,i}
#       describing weights between vertices of type i and those of type j
    
    # Additional optional input [param,value]:
#   clusters   maximum number of super vertices (default n)
#   numit       number of iterations numit (default 500)
#   abort       abort iteration if (normalized) change of cost function is 
#                   below this treshold (default 1e-10)
#   verbose     text output (default true)
#   verbosecompact  compact text output during counting (default true)
    
    # Output:
#   Summary   a summary k-paritite super graph
#   Cluster   clustering cell vector of the $k$-type vertices
#   fs  cost function value
    
    # Output is such that Aij-Ci*Bij*Cj' has minimal Frobenius norm.
    
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:22
    if logical_not(iscell(A)) or k < 2 or size(A,2) != k:
        error('need symmetric cell array as input of k-partite graph')
    
    # determine dimensions
    n=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:25
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            nn=size(A[i,j],1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:28
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:31
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',i,j)
            nn=size(A[j,i],2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:36
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:39
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',j,i)
    
    if min(n) == 0:
        error('need at least one connectivity matrix per column')
    
    p=copy(inputParser)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:48
    p.addParamValue('clusters',n,lambda x=None: isvector(x) and length(x) == k)
    p.addParamValue('numit',200,lambda x=None: x > 0 and mod(x,1) == 0)
    p.addParamValue('abort',1e-10,lambda x=None: x >= 0)
    p.addParamValue('verbose',true,islogical)
    p.addParamValue('verbosecompact',true,islogical)
    p.parse(varargin[arange()])
    res=p.Results
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:55
    if res.verbose:
        fprintf('starting graph Co-clustering of %i-partite graph with partition sizes: ',k)
        disp(n.T)
    
    ## (possibly) symmetrize or fill up lower (or upper) triangular part of A
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            if isempty(A[i,j]):
                A[i,j]=A[j,i].T
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:65
            else:
                if logical_not(isempty(A[j,i])):
                    A[i,j]=(A[i,j] + A[j,i].T) / 2
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:67
    
    maxcluster=res.clusters
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:71
    ## matrix containing empty connections implying NO fit here, includes diagonal!
    hasCon=zeros(k)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:73
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            hasCon[i,j]=logical_not(isempty(A[i,j]))
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:74
    
    flag=copy(true)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:75
    r=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:76
    for i in arange(1,k).reshape(-1):
        r[i]=maxcluster(i)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:78
    
    ## initialization
    t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:82
    B,C,powerA=Initilize(A,hasCon,n,r,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:83
    e=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:84
    disp('initilization time is')
    disp(e - t)
    while flag:

        B,C=Refinement(B,C,A,r,powerA,res,hasCon,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:88
        fold=objective(A,B,C,k,hasCon)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:90
        fprintf('old objective value is %f: \n',fold)
        disp(sum(r))
        disp(sum(maxcluster))
        if sum(r) > sum(maxcluster):
            Cluster,Summary,r,isreduced=search(B,C,k,r,true,maxcluster,nargout=4)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:95
        else:
            Cluster,Summary,r,isreduced=search(B,C,k,r,false,maxcluster,nargout=4)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:97
        if isreduced:
            Summary,Cluster=Refinement(Summary,Cluster,A,r,powerA,res,hasCon,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:100
        fnew=objective(A,Summary,Cluster,k,hasCon)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:102
        fprintf('new objective value is %f: \n',fnew)
        disp(r.T)
        if abs(fnew - fold) / powerA < res.abort:
            flag=copy(false)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:106
        B=copy(Summary)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:108
        C=copy(Cluster)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:109

    
    f=objective(A,Summary,Cluster,k,hasCon)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:111
    return Summary,Cluster,f
    
if __name__ == '__main__':
    pass
    
    
@function
def ClusterMinusmany(C=None,i=None,a=None,v=None,*args,**kwargs):
    varargin = ClusterMinusmany.varargin
    nargin = ClusterMinusmany.nargin

    M=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:115
    Cnew=copy(C)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:116
    n1,n2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:117
    nc=0
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:118
    isreduced=copy(false)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:119
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) <= v:
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:122
            isreduced=copy(true)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:123
    
    Mnew=zeros(n1,n2 - nc)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:126
    nc=1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:127
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) > v:
            Mnew[arange(),nc]=M(arange(),m1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:130
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:131
    
    Cnew[i,1]=Mnew
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:134
    return Cnew,isreduced
    
if __name__ == '__main__':
    pass
    
    
@function
def SummaryMinusmany(B=None,i=None,a=None,v=None,*args,**kwargs):
    varargin = SummaryMinusmany.varargin
    nargin = SummaryMinusmany.nargin

    Bnew=copy(B)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:138
    n1,n2=size(B,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:139
    nc=0
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:140
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) <= v:
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:143
    
    for j in arange(1,n1).reshape(-1):
        M=B[j,i]
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:147
        if size(M,2) < 1:
            continue
        b1,b2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:151
        Mnew=zeros(b1,b2 - nc)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:152
        idx=1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:153
        for m1 in arange(1,length(a)).reshape(-1):
            if a(m1) > v:
                Mnew[arange(),idx]=M(arange(),m1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:156
                idx=idx + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:157
        Bnew[j,i]=Mnew
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:160
    
    for j in arange(1,n2).reshape(-1):
        M=B[i,j]
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:163
        if size(M,1) < 1:
            continue
        b1,b2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:167
        Mnew=zeros(b1 - nc,b2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:168
        idx=1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:169
        for m1 in arange(1,length(a)).reshape(-1):
            if a(m1) > v:
                Mnew[idx,arange()]=M(m1,arange())
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:172
                idx=idx + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:173
        Bnew[i,j]=Mnew
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:176
    
    
    return Bnew
    
if __name__ == '__main__':
    pass
    
    #Cross-many out cluster reduction
    
@function
def search(B=None,C=None,k=None,r=None,ishard=None,maxcluster=None,*args,**kwargs):
    varargin = search.varargin
    nargin = search.nargin

    #search the best number of clusters for each type of vertices in k-partite
#graph A
# Input: 
# A: The cell array representation for k-partite graph
# B: The cell array representation for summary graph with number of
# supernodes r=[r1,r2, ...,rk]
# C: The cell array representation for node to cluster (super node)
# assignment
# r: current super node assignment r=[r1,r2,..., rk]
# Output:
# Cluster: New node to cluster assigment by cross-many out cluster reduction
# Summary: New summary graph by cross-many out cluster reduction
    isreduced=copy(false)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:195
    for i in arange(1,k).reshape(-1):
        fprintf('old number of clusters for %i-type is %i\n',i,r(i))
        if r(i) < max(3,maxcluster / k):
            fprintf('skip %i-type',i)
            continue
        ni=size(C[i,1],1)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:202
        a=zeros(1,r(i))
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:203
        for j in arange(1,ni).reshape(-1):
            __,index=max(C[i,1](j,arange()),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:205
            a[index]=a(index) + 1
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:206
        if ishard:
            v,__=min(a,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:209
        else:
            v=0
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:211
        C,isflag=ClusterMinusmany(C,i,a,v,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:213
        if isflag:
            isreduced=copy(true)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:215
        B=SummaryMinusmany(B,i,a,v)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:217
        r[i]=size(C[i,1],2)
# ../kpartite-graph-ER-master/matlab_code/Search_clustering/GraphSumPlus.m:218
        fprintf('new number of clusters for %i-type is %i\n',i,r(i))
    
    return C,B,r,isreduced
    
if __name__ == '__main__':
    pass
    
    # function [Cnew]=ClusterMinusone(C,i,m1)
#     Cnew=C;
#     M=C{i,1};
#     disp('matrix size');
#     disp(size(M,1));
#     disp(size(M,2));
#     disp(m1);
#     if size(M,2)>0
#         if (m1>1)&&(m1<size(M,2))
#             M=M(:,[1:m1-1,m1+1:end]);
#         elseif m1>1
#                 M=M(:,1:m1-1);
#         else
#                 M=M(:,m1+1:end);
#         end
#         Cnew{i,1}=M;
#     end
# end
# 
# function [Cnew]=SummaryMinusone(C,i,m1)
#      Cnew=C;
#      [n1,n2]=size(C);
#      for j=1:n1
#          M=C{j,i};
#          if size(M,2)>0
#              if (m1>1)&&(m1<size(M,2))
#                  M=M(:,[1:m1-1,m1+1:end]);
#              elseif m1>1
#                      M=M(:,1:m1-1);
#              else
#                      M=M(:,m1+1:end);
#              end
#              Cnew{j,i}=M;
#          end
#      end
#      for j=1:n2
#          M=C{i,j};
#          if size(M,1)>0
#              if (m1>1)&&(m1<size(M,1))
#                  M=M([1:m1-1,m1+1:end],:);
#              else if m1>1
#                      M=M(1:m1-1,:);
#                  else
#                      M=M(m1+1:end,:);
#                  end
#              end
#              Cnew{i,j}=M;
#          end
#      end
# end
    
    # function [Cluster, Summary, r]=search(A,B,C,k,hascon,r)
# #search the best number of clusters for each type of vertices in k-partite
# #graph A
# # Input: 
# # A: The cell array representation for k-partite graph
# # B: The cell array representation for summary graph with number of
# # supernodes r=[r1,r2, ...,rk]
# # C: The cell array representation for node to cluster (super node)
# # assignment
# # r: current super node assignment r=[r1,r2,..., rk]
# # Output:
# # Cluster: New node to cluster assigment by cross-one out merging
# # Summary: New summary graph by cross-one out merging
# fold=objective(A,B,C,k,hascon);
# for i=1: k
#     minvalue=fold;
#     Cluster=C;
#     Summary=B;
#     fprintf('old number of clusters for #i-type is #i\n',i,r(i));
#     for m1=1:r(i)
#         [Cnew]=ClusterMinusone(C,i,m1);
#         [Bnew]=SummaryMinusone(B,i,m1);
#         flocalnew=objective(A,Bnew,Cnew,k,hascon);
#         if minvalue-flocalnew>0.000001
#             minvalue=flocalnew;
#             Cluster=Cnew;
#             Summary=Bnew;
#         end
#     end
#     C=Cluster;
#     B=Summary;
#     r(i)=size(C{i,1},2);
#     fprintf('new number of clusters for #i-type is #i\n',i,r(i));
#     fold=minvalue;
# end     
# end
# 
# function [Cnew]=ClusterMinusone(C,i,m1)
#     Cnew=C;
#     M=C{i,1};
#     if size(M,1)>0
#         M=M(:,[1:m1 - 1, m1 + 1:end]);
#         Cnew{i,1}=M;
#     end
# end
# 
# function [Cnew]=SummaryMinusone(C,i,m1)
#      Cnew=C;
#      [n1,n2]=size(C);
#      for j=1:n1
#          M=C{j,i};
#          if size(M,1)>0
#               M=M(:,[1:m1 - 1, m1 + 1:end]);
#               Cnew{j,i}=M;
#          end
#      end
#      for j=1:n2
#          M=C{i,j};
#          if size(M,1)>0
#              M=M([1:m1 - 1, m1 + 1:end],:);
#              Cnew{i,j}=M;
#          end
#      end
# end
# function [Cluster, Summary]=search(A,B,C,k,hascon,r)
# #search the best number of clusters for each type of vertices in k-partite
# #graph A
# # Input: 
# # A: The cell array representation for k-partite graph
# # B: The cell array representation for summary graph with number of
# # supernodes r=[r1,r2, ...,rk]
# # C: The cell array representation for node to cluster (super node)
# # assignment
# # r: current super node assignment r=[r1,r2,..., rk]
# # Output:
# # Cluster: New node to cluster assigment by best-two merging
# # Summary: New summary graph by best-two merging
# fold=objective(A,B,C,k,hascon);
# for i=1: k
#     minvalue=fold;
#     Cluster=C;
#     Summary=B;
#     for m1=1:r(i)-1
#         
#         for m2=(m1+1):r(i)
#             [Cnew]=merge(C,i,m1,m2,true);
#             [Bnew]=merge(B,i,m1,m2,false);
#             flocalnew=objective(A,Bnew,Cnew,k,hascon);
#             if flocalnew < minvalue
#                 minvalue=flocalnew;
#                 Cluster=Cnew;
#                 Summary=Bnew;
#             end
#         end
#     end
#     C=Cluster;
#     B=Summary;
#     [~,r(i)]=size(C{i,1});
#     fold=minvalue;
# end     
# end
    
    # function [Cnew]=merge(C,i,m1,m2,column)
#     [~,n2]=size(C);
#     Cnew=C;
#     for j=1:n2
#         M=C{i,j};
#         if size(M,1)>0
#             if column 
#                 M(:,m1)=M(:,m1)+M(:,m2);
#                 M=M(:,[1:m2 - 1, m2 + 1:end]);
#             else
#                 M(m1,:)=M(m1,:)+M(m2,:);
#                 M=M([1:m2 - 1, m2 + 1:end],:);
#             end
#             Cnew{i,j}=M;
#         end
#     end
# end
    