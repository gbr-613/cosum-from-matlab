# Generated with SMOP  0.41-beta
from smop.libsmop import *
#sys.path.append('../../')
sys.path.append('c:/Users/GBR-6/PROJECTS/CoSum/cosum/')
from gbr_lib import *
from IOfunction.normalize_X import normalize_X
from IOfunction.SaveclusterSparse import SaveclusterSparse
from Basic_clustering.GraphSum import GraphSum


def main():
    disp('Demo for product entity resolution with Basic Graph Summarization')
    rng(1234567)
    t=copy(cputime())
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:3
    product2attr = load_array("product2attr.txt")
    G = spconvert(product2attr)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:5
    G = normalize_X(G)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:7
    n=size(G,1)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:8
    product2product = load_array("product2product.txt")
    #sim=sparse(product2product(arange(),1),product2product(arange(),2),product2product(arange(),3),n,n)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:10
    sim=normalize_X(product2product) # sim=normalize_X(sim)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:11
    A=cell(2,2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:12
    A[1,1]=sim
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:13
    A[1,2]=G
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:14
    IOtime=cputime() - t
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:15

    clusters_number = 10
    for cx in range(1, 6):
        clusters_number = clusters_number * 2
        clusters_number_str = str(clusters_number)
        t = copy(cputime())
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:16
        __, C, __ = GraphSum(A, 'clusters', concat([clusters_number, clusters_number]), nargout=3)
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:17
        name = cell(2, 1)
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:18
        name[1, 1] = "productcluster-" + clusters_number_str + ".txt"
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:19
        name[2, 1] = 'attrcluster-' + clusters_number_str + '.txt'
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:20
        SaveclusterSparse(C, name)
        e = cputime - t
        # ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproduct.m:22
        disp('super node 20 running time')
        disp(e + IOtime)


if __name__ == '__main__':
    main()
