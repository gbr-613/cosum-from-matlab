# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 
def main():
    disp('Demo for product entity resolution with Advanced Graph Summarization')
    rng(1234567)
    t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:3
    load(product2attr.txt)
    G=spconvert(product2attr)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:5
    clear('product2attr')
    addpath(genpath('../../../'))
    G=G / max(max(G))
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:8
    n=size(G,1)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:9
    m=size(G,2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:10
    load(product2product.txt)
    sim=sparse(product2product(arange(),1),product2product(arange(),2),product2product(arange(),3),n,n)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:12
    sim=normalize_X(sim)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:13
    clear('product2product')
    load(attr2attr.txt)
    sim2=sparse(attr2attr(arange(),1),attr2attr(arange(),2),attr2attr(arange(),3),m,m)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:16
    sim2=normalize_X(sim2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:17
    A=cell(2,2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:18
    A[1,1]=sim
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:19
    A[1,2]=G
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:20
    A[2,2]=sim2
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:21
    __,C,__=GraphSumPlus(A,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:22
    name=cell(2,1)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:23
    name[1,1]='productcluster-search.txt'
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:24
    name[2,1]='attrcluster-search.txt'
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:25
    SaveclusterSparse(C,name)
    e=cputime - t
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runproductplus.m:27
    disp(e)


if __name__ == '__main__':
    main()