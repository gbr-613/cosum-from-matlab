# Generated with SMOP  0.41-beta
from smop.libsmop import *
#
def main():
    rng(1234567)
    t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:2
    load(product2attr.txt)
    G=spconvert(product2attr)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:4
    addpath(genpath('../../../'))
    G=G / max(max(G))
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:6
    n=size(G,1)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:7
    load(product2product.txt)
    sim=sparse(product2product(arange(),1),product2product(arange(),2),product2product(arange(),3),n,n)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:9
    sim=normalize_X(sim)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:10
    load(attr2attr.txt)
    sim2=sparse(attr2attr(arange(),1),attr2attr(arange(),2),attr2attr(arange(),3),m,m)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:12
    sim2=normalize_X(sim2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:13
    A=cell(2,2)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:14
    A[1,1]=sim
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:15
    A[1,2]=G
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:16
    A[2,2]=sim2
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:17
    thresall=(arange(0.1,1,0.1))
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:18
    e=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:19
    Iotime=e - t
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:20
    for i in arange(1,length(thresall)).reshape(-1):
        t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:22
        thres=thresall(i)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:23
        B=RandomSample(A,1,thres,n)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:24
        __,C,__=GraphSumPlus(B,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:25
        e=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/demo_code/Product/runscale.m:26
        fprintf('total running time for %d pencent of data',dot(10,i))
        disp(e - t + Iotime)


if __name__ == '__main__':
    main()
