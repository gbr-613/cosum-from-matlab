import random
import datetime
import numpy as np
import scipy
import scipy.sparse
import sys
import smop.libsmop


_start_time = datetime.datetime.now()


def rng(x):
    random.seed(a=x)


def cputime():
    _current_time = datetime.datetime.now()
    return _current_time - _start_time


def load_array(filename):
    file = open(filename,"r")
    result = []
    while True:
        line = file.readline()
        if not line:
            break
        words = list(map(lambda s: int(s) if s.find('.') == -1 else float(s), line.split()))
        result.append(words)
    result = np.array(result)     
    return result


def sparse(*argv):
    arg_len = len(argv)
    if arg_len > 5 or arg_len == 0 or arg_len == 4:
        raise SyntaxError("Unsupported number of arguments: " + str(arg_len))
    if arg_len == 5:
        i = argv[0]
        j = argv[1]
        s = argv[2]
        m = argv[3]
        n = argv[4]
        res = scipy.sparse.csr_matrix((s, (i, j)), [(m, n)])
    if arg_len == 3:
        i = argv[0]
        j = argv[1]
        s = argv[2]
        res = scipy.sparse.csr_matrix((s, (i, j)))
    if arg_len == 2:
        return sparse([], [], [], argv[0], argv[1])
    if arg_len == 1:
        s = argv[0]
        res = scipy.sparse.csr_matrix(s)
    res.eliminate_zeros()
    return res




def spconvert(a):
    if not isinstance(a, np.ndarray):
        a = np.array(a)
    return sparse(a)


def addpath(p):
    sys.path.append(p)

'''
def sqrt(a):
    if a.ndim  == 1:
        a = list(map(lambda s: s*s, a))
    else:
        a = list(map(lambda s: sqrt(s), a))
    return a
'''


def sqrt(a):
    if not isinstance(a, np.ndarray):
        a = np.array(a)
    if a.ndim == 1:
        a = map(lambda s: s*s, a)
    else:
        a = map(lambda s: sqrt(s), a)
    a = list(a)
    a = np.array(a)
    return a


def repmat(a, *d):
    if not isinstance(a, np.ndarray):
        a = np.array(a)
    if isinstance(d[0], tuple):
        res = np.tile(a, d[0])
    else:
        res = np.tile(a, d)
    return res


def full(s):
    if isinstance(s, np.ndarray):
        return s
    return scipy.sparse.dia_matrix.toarray(s)


def fclose(f):
    f.close()


def eye(*argv):
    return np.eye(argv)


def iscell(a):
    print(type(a))
    if isinstance(a, smop.libsmop.cellarray):
        return True
    if not isinstance(a, np.ndarray):
        return False
    it = np.nditer([a])
    t = type(it[0])
    while not it.finished:
        if t != type(it[0]):
            return False
        it.iternext()
    return True


def error(s, *argv):
    # another bug in smop, overwrite the function
    raise(s % tuple(argv))


class inputParser:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def addParamValue(self, k, v):
        setattr(self, k, v)

    def Results(self):
        return self

'''
#a = [1,2,3,4]
a = [[1,2], [3,4]]
a = spconvert(a)
print(a)
print(" ---")
b = repmat(a,(3,2))
#b = np.tile(a,(2,3))
print(b)
print(" ---")
b = np.tile(a,(3,2))
print(b)


print("hello")
a = [1,2,3,4]
b = sqrt(a)
print(a)
print(b)
'''

