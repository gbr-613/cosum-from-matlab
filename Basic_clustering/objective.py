# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def objective(A=None,B=None,C=None,k=None,hasCon=None,*args,**kwargs):
    varargin = objective.varargin
    nargin = objective.nargin

    # compute the square loss between original graph and reconstructed graph
    f=0
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/objective.m:3
    for i in arange(1,k).reshape(-1):
        for j in arange(i + 1,k).reshape(-1):
            if hasCon(i,j):
                dz=A[i,j] - dot(dot(C[i],B[i,j]),C[j].T)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/objective.m:7
                f=f + sum(sum(dz ** 2))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/objective.m:8
    
    return f
    
if __name__ == '__main__':
    pass
    