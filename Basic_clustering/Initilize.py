# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Initilize(A=None,hasCon=None,n=None,r=None,*args,**kwargs):
    varargin = Initilize.varargin
    nargin = Initilize.nargin

    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:2
    B=cell(k,k)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:3
    C=cell(k,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:3
    powerA=0
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:3
    maxcluster=max(r)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:4
    
    if maxcluster < 50:
        for i in arange(1,k).reshape(-1):
            C[i]=max(randn(n(i),r(i)),1e-05)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:8
            C[i]=C[i] / repmat(sum(C[i],2),1,r(i))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:9
    else:
        for i in arange(1,k).reshape(-1):
            C[i]=zeros(n(i),r(i))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:13
            if hasCon(i,i):
                G=A[i,i]
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:15
                G=G + G.T
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:16
                S,CC=conncomp(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:17
                disp('connected component')
                disp(S)
                CC=CC.T
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:20
                if S > 5:
                    C[i]=CCInit(C[i],CC)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:22
                else:
                    C[i]=MYRandomInit(C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:24
            else:
                C[i]=MYRandomInit(C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:27
            C[i]=sparse(C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:29
    
    for i in arange(1,k).reshape(-1):
        if hasCon(i,i):
            C[i]=LP(A[i,i],C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:34
    
    C[i]=sparse(C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:37
    for i in arange(1,k).reshape(-1):
        for j in arange(i,k).reshape(-1):
            if hasCon(i,j):
                if j != i:
                    B[i,j]=max(randn(r(i),r(j)),1e-07)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:42
                    B[i,j]=sparse(B[i,j])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:43
                else:
                    B[i,j]=eye(r(i),r(j))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:45
                powerA=powerA + sum(sum(A[i,j] ** 2))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:47
    
    return B,C,powerA
    
if __name__ == '__main__':
    pass
    
    
@function
def LP(S=None,x=None,*args,**kwargs):
    varargin = LP.varargin
    nargin = LP.nargin

    y=copy(x)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:54
    for iter in arange(1,80).reshape(-1):
        y=dot(dot(0.5,S),y) + dot(0.5,x)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:56
        y=y / repmat(sum(y,2),1,size(y,2))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:57
    
    y=y / 0.5
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:59
    return y
    
if __name__ == '__main__':
    pass
    
    
@function
def MYRandomInit(Y=None,*args,**kwargs):
    varargin = MYRandomInit.varargin
    nargin = MYRandomInit.nargin

    n=size(Y,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:63
    r=size(Y,2)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:64
    for j in arange(1,n).reshape(-1):
        index=round(dot(rand(),r))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:66
        if index < 1:
            index=1
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:68
        if index > r:
            index=mod(index,r)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:71
        Y[j,index]=1
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:73
    
    return Y
    
if __name__ == '__main__':
    pass
    
    
@function
def CCInit(Y=None,CC=None,*args,**kwargs):
    varargin = CCInit.varargin
    nargin = CCInit.nargin

    n=size(Y,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:79
    r=size(Y,2)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:80
    for j in arange(1,n).reshape(-1):
        idx=round(CC(j))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:82
        if idx > r:
            idx=mod(idx,r)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:84
        if idx == 0:
            idx=1
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:87
        Y[j,idx]=1
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:89
    
    return Y
    
if __name__ == '__main__':
    pass
    
    
@function
def conncomp(G=None,*args,**kwargs):
    varargin = conncomp.varargin
    nargin = conncomp.nargin

    p,__,r=dmperm(G.T + speye(size(G)),nargout=3)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:94
    S=numel(r) - 1
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:95
    C=cumsum(full(sparse(1,r(arange(1,end() - 1)),1,1,size(G,1))))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:96
    C[p]=C
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Initilize.m:97
    return S,C
    
if __name__ == '__main__':
    pass
    