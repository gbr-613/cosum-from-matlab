# Generated with SMOP  0.41-beta
from smop.libsmop import *
from gbr_lib import *

    
@function
def GraphSum(A=None,varargin=None,*args,**kwargs):
    varargin = GraphSum.varargin
    nargin = GraphSum.nargin

    # function [B,C,fs]=GraphSum(A,[param,value])
    
    # Input: 
#   k-partite graph A, given as cell array with matrix A{i,j}=A{j,i}
#       describing weights between vertices of color i and those of color j
    
    # Additional optional input [param,value]:
#   clusters    number of vertex clusters of given color (default [2 ...2])
#   numit       number of iterations numit (default 1000)
#   abort       abort iteration if (normalized) change of cost function is 
#                   below this treshold (default 1e-10)
#   verbose     text output (default true)
#   verbosecompact  compact text output during counting (default true)
    
    # Output:
#   B   k-partite summary graph
#   C   the mapping between original vertices and super nodes in summary graph
#   fs  cost function value in each iteration
    
    # Output is such that Aij-Ci*Bij*Cj' has minimal Frobenius norm.
    
    
    # EXAMPLE:
# Tripartite graph
# A=cell(3,3);
# A{1,2}=[1 1 1 1; 1 1 1 1; 0 0 1 1; 0 0 1 1;0 0 1 1;0 0 1 1; 1 1 0 0 ];
# A{1,3}=[1 1 0 0; 1 1 0 0; 1 1 0 0; 1 1 0 0;1 1 1 1;1 1 1 1; 1 1 1 1 ];
# [B,C,cost] = GraphSum(A,'clusters',[3,2,2]);
    
    ## input parameter scan
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:35
    if logical_not(iscell(A)) or k < 2 or size(A,2) != k:
        error('need symmetric cell array as input of k-partite graph')
    
    # determine dimensions
    n=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:38
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            nn=size(A[i,j],1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:41
            if nn > 0:
                if n[i] == 0:  # grisha bug if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:44
                else:
                    if n[i] != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',i,j)
            nn=size(A[j,i],2)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:49
            if nn > 0:
                if n[i] == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:52
                else:
                    if n[i] != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',j,i)
    
    if min(n) == 0:
        error('need at least one connectivity matrix per column')
    
    p = inputParser(**kwargs) # grisha bug p=copy(inputParser)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:61
    p.addParamValue('clusters', repmat(2,k,1))#  bug? ,lambda x=None: isvector(x) and length(x) == k)
    p.addParamValue('numit', 200)#  bug? , lambda x=None: x > 0 and mod(x,1) == 0)
    p.addParamValue('abort', 1e-10)#  bug? , lambda x=None: x >= 0)
    p.addParamValue('verbose', true)#  bug? , islogical)
    p.addParamValue('verbosecompact', true)#  bug? , islogical)
    # grisha bug?  p.parse(varargin[arange()])
    res = p.Results()
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:68
    r = res.clusters
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:69
    if res.verbose:
        print('starting summarizing of %i-partite graph with partition sizes: ' % k) # bug? fprintf('starting summarizing of %i-partite graph with partition sizes: ',k)
        disp(n.T)
    
    ## (possibly) symmetrize or fill up lower (or upper) triangular part of A
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            if isempty(A[i,j]):
                A[i,j]=A[j,i].T
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:79
            else:
                if logical_not(isempty(A[j,i])):
                    A[i,j]=(A[i,j] + A[j,i].T) / 2
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:81
    
    ## matrix containing empty connections implying NO fit here, includes diagonal!
    hasCon=zeros(k)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:87
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            hasCon[i,j]=logical_not(isempty(A[i,j]))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:88
    
    ## initialization
    s=copy(cputime())
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:91
    B,C,powerA=Initilize(A,hasCon,n,r,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:92
    t=copy(cputime())
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:93
    disp('Initilization time ')
    disp(t - s)
    B,C,fs=Refinement(B,C,A,r,powerA,res,hasCon,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/GraphSum.m:97
    if res.verbose:
        fprintf('\n')

    return B, C, fs
