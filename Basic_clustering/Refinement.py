# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Refinement(B=None,C=None,A=None,r=None,powerA=None,res=None,hasCon=None,*args,**kwargs):
    varargin = Refinement.varargin
    nargin = Refinement.nargin

    #  
## iteration
    numchar=0
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:4
    fs=zeros(1,res.numit)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:5
    myzero=1e-18
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:6
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:7
    for it in arange(1,res.numit).reshape(-1):
        for i in arange(1,k).reshape(-1):
            N=zeros(size(C[i]))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:10
            D=zeros(size(C[i]))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:11
            for j in arange(i,k).reshape(-1):
                if hasCon(i,j):
                    if j != i:
                        N=N + dot(dot(A[i,j],C[j]),B[i,j].T)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:15
                        D=D + dot(dot(dot(dot(C[i],B[i,j]),C[j].T),C[j]),B[i,j].T)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:16
                    else:
                        N=N + dot(A[i,j],C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:18
                        D=D + dot(diag(sum(A[i,j])),C[i])
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:19
            C[i]=multiply(C[i],N) / max(D,myzero)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:23
            C[i]=C[i] / repmat(sum(C[i],2) + myzero,1,r(i))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:25
            #         t=cputime;
#         disp('normalization time');
#         disp(t-s);
        for i in arange(1,k).reshape(-1):
            for j in arange(i + 1,k).reshape(-1):
                if hasCon(i,j):
                    B[i,j]=multiply(B[i,j],(dot(dot(C[i].T,A[i,j]),C[j]))) / max(dot(dot(dot(dot(C[i].T,C[i]),B[i,j]),C[j].T),C[j]),myzero)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:34
                    B[j,i]=B[i,j].T
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:35
        f=0
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:39
        for i in arange(1,k).reshape(-1):
            for j in arange(i,k).reshape(-1):
                if hasCon(i,j):
                    if j != i:
                        f=f + sum(sum((A[i,j] - dot(dot(C[i],B[i,j]),C[j].T)) ** 2))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:44
                    else:
                        f=f + sum(sum((A[i,i] - dot(C[i],C[i].T)) ** 2))
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:46
        fs[it]=f
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:51
        if res.verbose:
            if res.verbosecompact:
                for i in arange(1,numchar).reshape(-1):
                    fprintf('\b')
            else:
                fprintf('\n')
            s=sprintf('it=%i  cost=%8.4f',it,f)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:58
            numchar=length(s)
# ../kpartite-graph-ER-master/matlab_code/Basic_clustering/Refinement.m:59
            fprintf(s)
        if it > 1 and (fs(it - 1) - fs(it)) / powerA < res.abort:
            if res.verbose:
                fprintf('\ncovergence after %i iterations\n',it)
            break
    return B, C, fs
