# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Plottrigraph(A=None,pattern=None,color=None,color2=None,center=None,*args,**kwargs):
    varargin = Plottrigraph.varargin
    nargin = Plottrigraph.nargin

    rng(1357)
    #A the input tripartite graph
    k1,k2=size(A,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:4
    if k1 != k2:
        disp('the dimension does not aggree in k-partite graph')
    
    k=copy(k1)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:8
    nodesize=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:9
    xmin=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:10
    xmin[1]=center + 0
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:11
    xmin[2]=center - 0.02
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:12
    xmin[3]=center + 0.02
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:13
    
    # if mod(i,2)==1
          # xmin(i)=center+0.01*(i-1);
      # else
          # xmin(i)=center-0.01*(i-1);
      # end
  # end
    for i in arange(1,k - 1).reshape(-1):
        for j in arange((i + 1),k).reshape(-1):
            G=A[i,j]
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:23
            ni,nj=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:24
            if ni > 1 and nj > 1:
                nodesize[i]=ni
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:26
                nodesize[j]=nj
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:27
    
    
    #figure
    hold('on')
    #plot the node,
 #remember the position of each node
    Ycor=cell(k,1)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:36
    for i in arange(1,k).reshape(-1):
        Ycor[i,1]=zeros(nodesize(i),1)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:38
    
    for i in arange(1,k).reshape(-1):
        for n1 in arange(1,nodesize(i)).reshape(-1):
            if i != k:
                ypos=n1 + rand / 2
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:43
            else:
                ypos=n1 / 1.5 + rand / 2
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:45
            Ycor[i,1][n1]=ypos
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:47
            plot(xmin(i),ypos,pattern,'MarkerSize',25,'color',color[i,1])
    
    
    #plot the edge
    for i in arange(1,k - 1).reshape(-1):
        for j in arange((i + 1),k).reshape(-1):
            G=A[i,j]
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:55
            ni,nj=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:56
            if ni > 1 and nj > 1:
                for n1 in arange(1,ni).reshape(-1):
                    for n2 in arange(1,nj).reshape(-1):
                        if G(n1,n2) > 0.01:
                            plot(concat([xmin(i),xmin(j)]),concat([Ycor[i,1](n1),Ycor[j,1](n2)]),'Color',color2,'LineWidth',1.4)
                    __,index=max(G(n1,arange()),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plottrigraph.m:64
                    plot(concat([xmin(i),xmin(j)]),concat([Ycor[i,1](n1),Ycor[j,1](index)]),'Color',color2,'LineWidth',1.4)
    
    axis('off')
    return xmin,Ycor
    
if __name__ == '__main__':
    pass
    