# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def TriclusterandVisual(A=None,clusternum=None,*args,**kwargs):
    varargin = TriclusterandVisual.varargin
    nargin = TriclusterandVisual.nargin

    B,C,cost=fuzzygraphclustering(A,'clusters',clusternum,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/visulization/TriclusterandVisual.m:2
    h=copy(figure)
# ../kpartite-graph-ER-master/matlab_code/visulization/TriclusterandVisual.m:3
    set(gcf,'color','w')
    x1,y1=Plottrigraph(A,'.','b',concat([0.7,0.7,0.7]),0,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/TriclusterandVisual.m:5
    x2,y2=Plottrigraph(B,'s','r',concat([0,0,0]),- 0.1,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/TriclusterandVisual.m:6
    Plotnode2cluster(C,x1,x2,y1,y2)
    saveas(h,'Tritoy','fig')
    saveas(h,'Tritoy','png')
    return cost
    
if __name__ == '__main__':
    pass
    