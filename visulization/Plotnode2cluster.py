# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Plotnode2cluster(C=None,Xmin1=None,Xmin2=None,Ycor1=None,Ycor2=None,color=None,*args,**kwargs):
    varargin = Plotnode2cluster.varargin
    nargin = Plotnode2cluster.nargin

    #Xmin1: the x coordiante of nodes
#Ycor1: the y coordiante of nodes
#Xmin2: the x coordinate of clusters
#Ycor2: the y coordinate of clusters
    k,k2=size(C,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plotnode2cluster.m:6
    for i in arange(1,k).reshape(-1):
        G=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/visulization/Plotnode2cluster.m:8
        ni,nj=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/visulization/Plotnode2cluster.m:9
        for n1 in arange(1,ni).reshape(-1):
            for n2 in arange(1,nj).reshape(-1):
                if G(n1,n2) > 0.1:
                    plot(concat([Xmin1(i),Xmin2(i)]),concat([Ycor1[i,1](n1),Ycor2[i,1](n2)]),'Color',color[i,1],'LineWidth',G(n1,n2))
    
    return
    
if __name__ == '__main__':
    pass
    