# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

@function
def weightedobjective(A=None,B=None,C=None,k=None,hasCon=None,w=None,*args,**kwargs):
    varargin = weightedobjective.varargin
    nargin = weightedobjective.nargin

    # compute the square loss between original graph and reconstructed graph
    f=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/weightedobjective.m:3
    for i in arange(1,k).reshape(-1):
        for j in arange(i + 1,k).reshape(-1):
            if hasCon(i,j):
                dz=dot(w(i,j),(A[i,j] - dot(dot(C[i],B[i,j]),C[j].T)))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/weightedobjective.m:7
                f=f + sum(sum(dz ** 2))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/weightedobjective.m:8
    
    return f
    
if __name__ == '__main__':
    pass
    