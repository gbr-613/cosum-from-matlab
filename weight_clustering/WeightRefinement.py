# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def WeightRefinement(B=None,C=None,A=None,r=None,powerA=None,res=None,hasCon=None,w=None,*args,**kwargs):
    varargin = WeightRefinement.varargin
    nargin = WeightRefinement.nargin
## iteration
    numchar=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:4
    fs=zeros(1,res.numit)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:5
    myzero=1e-09
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:6
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:7
    for it in arange(1,res.numit).reshape(-1):
        for i in arange(1,k).reshape(-1):
            N=zeros(size(C[i]))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:10
            D=zeros(size(C[i]))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:11
            for j in arange(i,k).reshape(-1):
                if hasCon(i,j):
                    if j != i:
                        N=N + dot(dot(dot(w(i,j),A[i,j]),C[j]),B[i,j].T)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:15
                        D=D + dot(dot(dot(dot(dot(w(i,j),C[i]),B[i,j]),C[j].T),C[j]),B[i,j].T)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:16
                    else:
                        N=N + dot(dot(w(i,j),A[i,j]),C[i])
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:18
                        D=D + dot(dot(w(i,j),diag(sum(A[i,j]))),C[i])
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:19
            C[i]=multiply(C[i],sqrt(N / max(D,myzero)))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:23
            C[i]=C[i] / repmat(sum(C[i],2) + myzero,1,r(i))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:25
            #         t=cputime;
#         disp('normalization time');
#         disp(t-s);
        for i in arange(1,k).reshape(-1):
            for j in arange(i + 1,k).reshape(-1):
                if hasCon(i,j):
                    if j != i:
                        B[i,j]=multiply(B[i,j],sqrt((dot(dot(C[i].T,A[i,j]),C[j])) / max(dot(dot(dot(dot(C[i].T,C[i]),B[i,j]),C[j].T),C[j]),myzero)))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:35
                        B[j,i]=B[i,j].T
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:36
        f=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:41
        for i in arange(1,k).reshape(-1):
            for j in arange(i,k).reshape(-1):
                if hasCon(i,j):
                    if j != i:
                        f=f + dot(w(i,j),sum(sum((A[i,j] - dot(dot(C[i],B[i,j]),C[j].T)) ** 2)))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:46
                    else:
                        f=f + dot(w(i,j),sum(sum((A[i,i] - dot(C[i],C[i].T)) ** 2)))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:48
        fs[it]=f
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:53
        if res.verbose:
            if res.verbosecompact:
                for i in arange(1,numchar).reshape(-1):
                    fprintf('\b')
            else:
                fprintf('\n')
            s=sprintf('it=%i  cost=%8.4f',it,f)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:60
            numchar=length(s)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/WeightRefinement.m:61
            fprintf(s)
        if it > 1 and (fs(it - 1) - fs(it)) / powerA < res.abort:
            if res.verbose:
                fprintf('\ncovergence after %i iterations\n',it)
            break
    return B, C, fs
