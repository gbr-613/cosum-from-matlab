# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def GraphWeightSumPlus(A=None,varargin=None,*args,**kwargs):
    varargin = GraphWeightSumPlus.varargin
    nargin = GraphWeightSumPlus.nargin

    
    # Input: 
#   k-partite graph A, given as cell array with matrix A{i,j}=A{j,i}
#       describing weights between vertices of type i and those of type j
    
    # Additional optional input [param,value]:
#   clusters   maximum number of super vertices (default n)
#   numit       number of iterations numit (default 500)
#   abort       abort iteration if (normalized) change of cost function is 
#                   below this treshold (default 1e-10)
#   verbose     text output (default true)
#   verbosecompact  compact text output during counting (default true)
    
    # Output:
#   Summary   a summary k-paritite super graph
#   Cluster   clustering cell vector of the $k$-type vertices
#   fs  cost function value
    
    # Output is such that Aij-Ci*Bij*Cj' has minimal Frobenius norm.
    
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:22
    if logical_not(iscell(A)) or k < 2 or size(A,2) != k:
        error('need symmetric cell array as input of k-partite graph')
    
    # determine dimensions
    n=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:25
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            nn=size(A[i,j],1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:28
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:31
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',i,j)
            nn=size(A[j,i],2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:36
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:39
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',j,i)
    
    if min(n) == 0:
        error('need at least one connectivity matrix per column')
    
    p=copy(inputParser)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:48
    p.addParamValue('clusters',n,lambda x=None: isvector(x) and length(x) == k)
    p.addParamValue('weights',repmat(1,k,k),lambda x=None: ismatrix(x) and size(x,1) == k and size(x,2) == k)
    p.addParamValue('numit',200,lambda x=None: x > 0 and mod(x,1) == 0)
    p.addParamValue('abort',1e-10,lambda x=None: x >= 0)
    p.addParamValue('verbose',true,islogical)
    p.addParamValue('verbosecompact',true,islogical)
    p.parse(varargin[arange()])
    res=p.Results
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:56
    w=res.weights
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:57
    if res.verbose:
        fprintf('starting graph Co-clustering of %i-partite graph with partition sizes: ',k)
        disp(n.T)
    
    ## (possibly) symmetrize or fill up lower (or upper) triangular part of A
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            if isempty(A[i,j]):
                A[i,j]=A[j,i].T
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:67
            else:
                if logical_not(isempty(A[j,i])):
                    A[i,j]=(A[i,j] + A[j,i].T) / 2
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:69
    
    maxcluster=res.clusters
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:73
    ## matrix containing empty connections implying NO fit here, includes diagonal!
    hasCon=zeros(k)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:75
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            hasCon[i,j]=logical_not(isempty(A[i,j]))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:78
    
    flag=copy(true)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:81
    r=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:82
    for i in arange(1,k).reshape(-1):
        r[i]=maxcluster(i)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:84
    
    ## initialization
    t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:88
    B,C,powerA=Initilize(A,hasCon,n,r,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:89
    e=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:90
    disp('initilization time is')
    disp(e - t)
    while flag:

        B,C=WeightRefinement(B,C,A,r,powerA,res,hasCon,w,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:94
        fold=weightedobjective(A,B,C,k,hasCon,w)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:96
        fprintf('old objective value is %f: \n',fold)
        disp(sum(r))
        disp(sum(maxcluster))
        if sum(r) > sum(maxcluster):
            Cluster,Summary,r,isreduced=search(B,C,k,r,true,maxcluster,nargout=4)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:101
        else:
            Cluster,Summary,r,isreduced=search(B,C,k,r,false,maxcluster,nargout=4)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:103
        if isreduced:
            Summary,Cluster=WeightRefinement(Summary,Cluster,A,r,powerA,res,hasCon,w,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:106
        fnew=weightedobjective(A,Summary,Cluster,k,hasCon,w)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:108
        fprintf('new objective value is %f: \n',fnew)
        disp(r.T)
        if abs(fnew - fold) / powerA < res.abort:
            flag=copy(false)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:112
        B=copy(Summary)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:114
        C=copy(Cluster)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:115

    
    f=weightedobjective(A,Summary,Cluster,k,hasCon,w)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:117
    return Summary,Cluster,f
    
if __name__ == '__main__':
    pass
    
    
@function
def ClusterMinusmany(C=None,i=None,a=None,v=None,*args,**kwargs):
    varargin = ClusterMinusmany.varargin
    nargin = ClusterMinusmany.nargin

    M=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:121
    Cnew=copy(C)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:122
    n1,n2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:123
    nc=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:124
    isreduced=copy(false)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:125
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) <= v:
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:128
            isreduced=copy(true)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:129
    
    Mnew=zeros(n1,n2 - nc)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:132
    nc=1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:133
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) > v:
            Mnew[arange(),nc]=M(arange(),m1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:136
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:137
    
    Cnew[i,1]=Mnew
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:140
    return Cnew,isreduced
    
if __name__ == '__main__':
    pass
    
    
@function
def SummaryMinusmany(B=None,i=None,a=None,v=None,*args,**kwargs):
    varargin = SummaryMinusmany.varargin
    nargin = SummaryMinusmany.nargin

    Bnew=copy(B)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:144
    n1,n2=size(B,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:145
    nc=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:146
    for m1 in arange(1,length(a)).reshape(-1):
        if a(m1) <= v:
            nc=nc + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:149
    
    for j in arange(1,n1).reshape(-1):
        M=B[j,i]
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:153
        if size(M,2) < 1:
            continue
        b1,b2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:157
        Mnew=zeros(b1,b2 - nc)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:158
        idx=1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:159
        for m1 in arange(1,length(a)).reshape(-1):
            if a(m1) > v:
                Mnew[arange(),idx]=M(arange(),m1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:162
                idx=idx + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:163
        Bnew[j,i]=Mnew
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:166
    
    for j in arange(1,n2).reshape(-1):
        M=B[i,j]
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:169
        if size(M,1) < 1:
            continue
        b1,b2=size(M,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:173
        Mnew=zeros(b1 - nc,b2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:174
        idx=1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:175
        for m1 in arange(1,length(a)).reshape(-1):
            if a(m1) > v:
                Mnew[idx,arange()]=M(m1,arange())
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:178
                idx=idx + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:179
        Bnew[i,j]=Mnew
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:182
    
    
    return Bnew
    
if __name__ == '__main__':
    pass
    
    #Cross-many out cluster reduction
    
@function
def search(B=None,C=None,k=None,r=None,ishard=None,maxcluster=None,*args,**kwargs):
    varargin = search.varargin
    nargin = search.nargin

    #search the best number of clusters for each type of vertices in k-partite
#graph A
# Input: 
# A: The cell array representation for k-partite graph
# B: The cell array representation for summary graph with number of
# supernodes r=[r1,r2, ...,rk]
# C: The cell array representation for node to cluster (super node)
# assignment
# r: current super node assignment r=[r1,r2,..., rk]
# Output:
# Cluster: New node to cluster assigment by cross-many out cluster reduction
# Summary: New summary graph by cross-many out cluster reduction
    isreduced=copy(false)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:201
    for i in arange(1,k).reshape(-1):
        fprintf('old number of clusters for %i-type is %i\n',i,r(i))
        if r(i) < max(3,maxcluster / k):
            fprintf('skip %i-type',i)
            continue
        ni=size(C[i,1],1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:208
        a=zeros(1,r(i))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:209
        for j in arange(1,ni).reshape(-1):
            __,index=max(C[i,1](j,arange()),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:211
            a[index]=a(index) + 1
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:212
        if ishard:
            v,__=min(a,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:215
        else:
            v=0
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:217
        C,isflag=ClusterMinusmany(C,i,a,v,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:219
        if isflag:
            isreduced=copy(true)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:221
        B=SummaryMinusmany(B,i,a,v)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:223
        r[i]=size(C[i,1],2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSumPlus.m:224
        fprintf('new number of clusters for %i-type is %i\n',i,r(i))
    
    return C,B,r,isreduced
    
if __name__ == '__main__':
    pass
    
    # function [Cnew]=ClusterMinusone(C,i,m1)
#     Cnew=C;
#     M=C{i,1};
#     disp('matrix size');
#     disp(size(M,1));
#     disp(size(M,2));
#     disp(m1);
#     if size(M,2)>0
#         if (m1>1)&&(m1<size(M,2))
#             M=M(:,[1:m1-1,m1+1:end]);
#         elseif m1>1
#                 M=M(:,1:m1-1);
#         else
#                 M=M(:,m1+1:end);
#         end
#         Cnew{i,1}=M;
#     end
# end
# 
# function [Cnew]=SummaryMinusone(C,i,m1)
#      Cnew=C;
#      [n1,n2]=size(C);
#      for j=1:n1
#          M=C{j,i};
#          if size(M,2)>0
#              if (m1>1)&&(m1<size(M,2))
#                  M=M(:,[1:m1-1,m1+1:end]);
#              elseif m1>1
#                      M=M(:,1:m1-1);
#              else
#                      M=M(:,m1+1:end);
#              end
#              Cnew{j,i}=M;
#          end
#      end
#      for j=1:n2
#          M=C{i,j};
#          if size(M,1)>0
#              if (m1>1)&&(m1<size(M,1))
#                  M=M([1:m1-1,m1+1:end],:);
#              else if m1>1
#                      M=M(1:m1-1,:);
#                  else
#                      M=M(m1+1:end,:);
#                  end
#              end
#              Cnew{i,j}=M;
#          end
#      end
# end
    
    # function [Cluster, Summary, r]=search(A,B,C,k,hascon,r)
# #search the best number of clusters for each type of vertices in k-partite
# #graph A
# # Input: 
# # A: The cell array representation for k-partite graph
# # B: The cell array representation for summary graph with number of
# # supernodes r=[r1,r2, ...,rk]
# # C: The cell array representation for node to cluster (super node)
# # assignment
# # r: current super node assignment r=[r1,r2,..., rk]
# # Output:
# # Cluster: New node to cluster assigment by cross-one out merging
# # Summary: New summary graph by cross-one out merging
# fold=objective(A,B,C,k,hascon);
# for i=1: k
#     minvalue=fold;
#     Cluster=C;
#     Summary=B;
#     fprintf('old number of clusters for #i-type is #i\n',i,r(i));
#     for m1=1:r(i)
#         [Cnew]=ClusterMinusone(C,i,m1);
#         [Bnew]=SummaryMinusone(B,i,m1);
#         flocalnew=objective(A,Bnew,Cnew,k,hascon);
#         if minvalue-flocalnew>0.000001
#             minvalue=flocalnew;
#             Cluster=Cnew;
#             Summary=Bnew;
#         end
#     end
#     C=Cluster;
#     B=Summary;
#     r(i)=size(C{i,1},2);
#     fprintf('new number of clusters for #i-type is #i\n',i,r(i));
#     fold=minvalue;
# end     
# end
# 
# function [Cnew]=ClusterMinusone(C,i,m1)
#     Cnew=C;
#     M=C{i,1};
#     if size(M,1)>0
#         M=M(:,[1:m1 - 1, m1 + 1:end]);
#         Cnew{i,1}=M;
#     end
# end
# 
# function [Cnew]=SummaryMinusone(C,i,m1)
#      Cnew=C;
#      [n1,n2]=size(C);
#      for j=1:n1
#          M=C{j,i};
#          if size(M,1)>0
#               M=M(:,[1:m1 - 1, m1 + 1:end]);
#               Cnew{j,i}=M;
#          end
#      end
#      for j=1:n2
#          M=C{i,j};
#          if size(M,1)>0
#              M=M([1:m1 - 1, m1 + 1:end],:);
#              Cnew{i,j}=M;
#          end
#      end
# end
# function [Cluster, Summary]=search(A,B,C,k,hascon,r)
# #search the best number of clusters for each type of vertices in k-partite
# #graph A
# # Input: 
# # A: The cell array representation for k-partite graph
# # B: The cell array representation for summary graph with number of
# # supernodes r=[r1,r2, ...,rk]
# # C: The cell array representation for node to cluster (super node)
# # assignment
# # r: current super node assignment r=[r1,r2,..., rk]
# # Output:
# # Cluster: New node to cluster assigment by best-two merging
# # Summary: New summary graph by best-two merging
# fold=objective(A,B,C,k,hascon);
# for i=1: k
#     minvalue=fold;
#     Cluster=C;
#     Summary=B;
#     for m1=1:r(i)-1
#         
#         for m2=(m1+1):r(i)
#             [Cnew]=merge(C,i,m1,m2,true);
#             [Bnew]=merge(B,i,m1,m2,false);
#             flocalnew=objective(A,Bnew,Cnew,k,hascon);
#             if flocalnew < minvalue
#                 minvalue=flocalnew;
#                 Cluster=Cnew;
#                 Summary=Bnew;
#             end
#         end
#     end
#     C=Cluster;
#     B=Summary;
#     [~,r(i)]=size(C{i,1});
#     fold=minvalue;
# end     
# end
    
    # function [Cnew]=merge(C,i,m1,m2,column)
#     [~,n2]=size(C);
#     Cnew=C;
#     for j=1:n2
#         M=C{i,j};
#         if size(M,1)>0
#             if column 
#                 M(:,m1)=M(:,m1)+M(:,m2);
#                 M=M(:,[1:m2 - 1, m2 + 1:end]);
#             else
#                 M(m1,:)=M(m1,:)+M(m2,:);
#                 M=M([1:m2 - 1, m2 + 1:end],:);
#             end
#             Cnew{i,j}=M;
#         end
#     end
# end
    