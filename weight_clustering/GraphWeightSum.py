# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def GraphWeightSum(A=None,varargin=None,*args,**kwargs):
    varargin = GraphWeightSum.varargin
    nargin = GraphWeightSum.nargin

    # function [B,C,fs]=GraphSum(A,[param,value])
    
    # Input: 
#   k-partite graph A, given as cell array with matrix A{i,j}=A{j,i}
#       describing weights between vertices of color i and those of color j
    
    # Additional optional input [param,value]:
#   clusters    number of vertex clusters of given color (default [2 ...2])
#   numit       number of iterations numit (default 1000)
#   weights     weights for different layers of graph (default [1, ... 1;... 1,...1])
#   abort       abort iteration if (normalized) change of cost function is 
#                   below this treshold (default 1e-10)
#   verbose     text output (default true)
#   verbosecompact  compact text output during counting (default true)
    
    # Output:
#   B   k-partite summary graph
#   C   the mapping between original vertices and super nodes in summary graph
#   fs  cost function value in each iteration
    
    # Output is such that Aij-Ci*Bij*Cj' has minimal Frobenius norm.
    
    
    # EXAMPLE:
# Tripartite graph
# A=cell(3,3);
# A{1,2}=[1 1 1 1; 1 1 1 1; 0 0 1 1; 0 0 1 1;0 0 1 1;0 0 1 1; 1 1 0 0 ];
# A{1,3}=[1 1 0 0; 1 1 0 0; 1 1 0 0; 1 1 0 0;1 1 1 1;1 1 1 1; 1 1 1 1 ];
# [B,C,cost] = GraphSum(A,'clusters',[3,2,2]);
    
    ## input parameter scan
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:36
    if logical_not(iscell(A)) or k < 2 or size(A,2) != k:
        error('need symmetric cell array as input of k-partite graph')
    
    # determine dimensions
    n=zeros(k,1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:39
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            nn=size(A[i,j],1)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:42
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:45
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',i,j)
            nn=size(A[j,i],2)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:50
            if nn > 0:
                if n(i) == 0:
                    n[i]=nn
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:53
                else:
                    if n(i) != nn:
                        error('need same sizes in k-partite graph description in matrix %i %i',j,i)
    
    if min(n) == 0:
        error('need at least one connectivity matrix per column')
    
    p=copy(inputParser)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:62
    p.addParamValue('clusters',n,lambda x=None: isvector(x) and length(x) == k)
    p.addParamValue('weights',repmat(1,k,k),lambda x=None: ismatrix(x) and size(x,1) == k and size(x,2) == k)
    p.addParamValue('numit',50,lambda x=None: x > 0 and mod(x,1) == 0)
    p.addParamValue('abort',1e-10,lambda x=None: x >= 0)
    p.addParamValue('verbose',true,islogical)
    p.addParamValue('verbosecompact',true,islogical)
    p.parse(varargin[arange()])
    res=p.Results
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:70
    r=res.clusters
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:71
    w=res.weights
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:72
    if res.verbose:
        fprintf('starting summarizing of %i-partite graph with partition sizes: ',k)
        disp(n.T)
    
    ## (possibly) symmetrize or fill up lower (or upper) triangular part of A
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            if isempty(A[i,j]):
                A[i,j]=A[j,i].T
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:82
            else:
                if logical_not(isempty(A[j,i])):
                    A[i,j]=(A[i,j] + A[j,i].T) / 2
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:84
    
    ## matrix containing empty connections implying NO fit here, includes diagonal!
    hasCon=zeros(k)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:90
    for i in arange(1,k).reshape(-1):
        for j in arange(1,k).reshape(-1):
            hasCon[i,j]=logical_not(isempty(A[i,j]))
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:93
    
    ## initialization
    s=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:98
    # disp(n);
# disp(r);
    B,C,powerA=Initilize(A,hasCon,n,r,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:101
    t=copy(cputime)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:102
    disp('Initilization time ')
    disp(t - s)
    # fs = 0;
# return;
#disp(C{2,1});
    B,C,fs=WeightRefinement(B,C,A,r,powerA,res,hasCon,w,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/weight_clustering/GraphWeightSum.m:108
    if res.verbose:
        fprintf('\n')
    