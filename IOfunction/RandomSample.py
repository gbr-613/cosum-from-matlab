# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def RandomSample(A=None,t=None,thres=None,n=None,*args,**kwargs):
    varargin = RandomSample.varargin
    nargin = RandomSample.nargin

    #given a k-type graph A, typte t, random sample thres*100# of vertices for t-type
#vertices
    k=size(A,1)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:4
    B=copy(A)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:5
    for i in arange(1,k).reshape(-1):
        if i == t:
            a=copy(n)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:8
            numelements=round(dot(thres,a))
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:10
            # get the randomly-selected indices
            indices=randperm(a)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:13
            indices=indices(arange(1,numelements))
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:14
            for j in arange(1,k).reshape(-1):
                G=A[i,j]
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:17
                a,b=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:18
                if a > 0 and b > 0:
                    # choose the subset of a you want
                    if j != i:
                        G=G(indices,arange())
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:22
                    else:
                        G=G(indices,indices)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:24
                    B[i,j]=G
# ../kpartite-graph-ER-master/matlab_code/IOfunction/RandomSample.m:26
    return B
    
    
if __name__ == '__main__':
    pass
    