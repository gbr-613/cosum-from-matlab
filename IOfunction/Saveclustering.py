# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Saveclustering(C=None,name=None,*args,**kwargs):
    varargin = Saveclustering.varargin
    nargin = Saveclustering.nargin

    k,__=size(C,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:2
    #layer 1: offer node
#layer 2: attr node
#layer 3: service node
#layer 4: web node
#layer 5: feature node
    for i in arange(1,k).reshape(-1):
        G=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:9
        if issparse(G):
            G=full(G)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:11
        n1,__=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:13
        fid=fopen(name[i,1],'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:14
        for j in arange(1,n1).reshape(-1):
            v,index=max(G(j,arange()),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering.m:16
            fprintf(fid,'%d\t%d\t%f\n',j,index,v)
        fclose(fid)
    
    return
    
if __name__ == '__main__':
    pass
    