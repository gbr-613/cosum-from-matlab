# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def SaveSummary(B=None,name=None,*args,**kwargs):
    varargin = SaveSummary.varargin
    nargin = SaveSummary.nargin

    
    #   Detailed explanation goes here
    n1,__=size(B,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:4
    n2,__=size(name,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:5
    if n1 != n2:
        disp('summary graph is not a square cell')
    
    #1: offer layer
#2: attribute layer
#3: Service layer
#4: Page layer
#5: Word layer
    for i in arange(1,n1).reshape(-1):
        for j in arange(1,n1).reshape(-1):
            G=B[i,j]
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:16
            a,b=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:17
            if a > 0:
                if (isempty(name[i,j]) == 0):
                    fprintf('%d %d %s\n',i,j,name[i,j])
                    fid=fopen(name[i,j],'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:21
                    for na in arange(1,a).reshape(-1):
                        value,index=max(G(na,arange()),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:23
                        fprintf(fid,'%d\t%d\t%f\n',na,index,value)
                    for nb in arange(1,b).reshape(-1):
                        value,index=max(G(arange(),nb),nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveSummary.m:27
                        fprintf(fid,'%d\t%d\t%f\n',index,nb,value)
    
    return
    
if __name__ == '__main__':
    pass
    