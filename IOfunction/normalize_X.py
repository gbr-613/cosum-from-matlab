# Generated with SMOP  0.41-beta
from smop.libsmop import *
from gbr_lib import *
import numpy as np

@function
def normalize_X(X=None,*args,**kwargs):
    varargin = normalize_X.varargin
    nargin = normalize_X.nargin

    X = full(X)  # grisha: allow libsmop work with it

    d = size(X,2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/normalize_X.m:2
    norms = sqrt(sum(X ** 2, 2))
# ../kpartite-graph-ER-master/matlab_code/IOfunction/normalize_X.m:3
    eps = 1e-09
# ../kpartite-graph-ER-master/matlab_code/IOfunction/normalize_X.m:4
    #X = np.divide(X, (repmat(norms,1,d) + eps)) # X = X / (repmat(norms,1,d) + eps)
    '''
    x0 = repmat(norms, 1, d)
    x0 = x0 + eps
    X = np.divide(X, x0)
    '''
    for i in list(range(1, norms.shape[0])):
        X[i][1] = X[i][1] / norms[i]
    X = sparse(X)


# ../kpartite-graph-ER-master/matlab_code/IOfunction/normalize_X.m:5
    #make sure the elements in X is nonnegative and X is orthogonal
    if min(min(X)) < 0:
        error('The entries cannot be negative')
    return X
