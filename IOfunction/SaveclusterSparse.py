# Generated with SMOP  0.41-beta
from smop.libsmop import *
from gbr_lib import *

    
@function
def SaveclusterSparse(C=None,name=None,*args,**kwargs):
    varargin = SaveclusterSparse.varargin
    nargin = SaveclusterSparse.nargin

    k,__=size(C,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:2
    for i in arange(1,k).reshape(-1):
        if isempty(name[i,1]) == 0:
            X=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:5
            p,q,value=find(X,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:6
            data_dump=concat([p,q,value])
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:7
            e=size(data_dump,1)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:8
            fid=fopen(name[i,1],'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/SaveclusterSparse.m:9
            for j in arange(1,e).reshape(-1):
                fprintf(fid,'%d\t%d\t%f\n',data_dump(j,1),data_dump(j,2),data_dump(j,3))
            fclose(fid)
    return
    
if __name__ == '__main__':
    pass
    