# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def Saveclustering_full(C=None,name=None,*args,**kwargs):
    varargin = Saveclustering_full.varargin
    nargin = Saveclustering_full.nargin

    k,__=size(C,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering_full.m:2
    for i in arange(1,k).reshape(-1):
        G=C[i,1]
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering_full.m:4
        G=full(G)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering_full.m:5
        n1,n2=size(G,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering_full.m:6
        fid=fopen(name[i,1],'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/Saveclustering_full.m:7
        for j in arange(1,n1).reshape(-1):
            for p in arange(1,n2).reshape(-1):
                if p != n2:
                    fprintf(fid,'%f\t',G(j,p))
                else:
                    fprintf(fid,'%f\n',G(j,p))
        fclose(fid)
    
    return
    
if __name__ == '__main__':
    pass
    