# Generated with SMOP  0.41-beta
from smop.libsmop import *
# 

    
@function
def saveCCSmatrix(X=None,filename=None,*args,**kwargs):
    varargin = saveCCSmatrix.varargin
    nargin = saveCCSmatrix.nargin

    i,j,value=find(X,nargout=3)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:2
    n1,n2=size(X,nargout=2)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:3
    nz=nnz(X)
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:4
    fid=fopen(strcat(filename,'_dim'),'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:5
    fprintf(fid,'%d\t%d\t%d\n',n1,n2,nz)
    fclose(fid)
    fid=fopen(strcat(filename,'_row_ccs'),'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:8
    for k in arange(1,length(i)).reshape(-1):
        fprintf(fid,'%d\n',i(k) - 1)
    
    fclose(fid)
    fid=fopen(strcat(filename,'_col_ccs'),'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:13
    for k in arange(1,length(j)).reshape(-1):
        fprintf(fid,'%d\n',j(k) - 1)
    
    fclose(fid)
    fid=fopen(strcat(filename,'_nz'),'w')
# ../kpartite-graph-ER-master/matlab_code/IOfunction/saveCCSmatrix.m:18
    for k in arange(1,length(value)).reshape(-1):
        fprintf(fid,'%f\n',value(k))
    
    fclose(fid)
    return
    
if __name__ == '__main__':
    pass
    